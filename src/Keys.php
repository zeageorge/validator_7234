<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

use JsonSerializable, Countable;
use zeageorge\validator_7234\rules\Key;
use function count, array_merge;

/**
 * Description of Keys
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Keys implements JsonSerializable, Countable {
  /** @var Key[] */
  protected $keys;

  /**
   * Constructor
   *
   * @param Key ...$keys
   */
  public function __construct(Key ...$keys) {
    $this->keys = $keys ?? [];
  }

  /**
   *
   * @return Key[]
   */
  public function toArray(): array {
    return $this->keys;
  }

  /**
   *
   * @param Key ...$keys
   * @return self
   */
  public function add(Key ...$keys): self {
    return $this->merge(new Keys(...$keys));
  }

  /**
   *
   * @return self
   */
  public function clear(): self {
    $this->keys = [];

    return $this;
  }

  /**
   *
   * @return int
   */
  public function count(): int {
    return count($this->keys);
  }

  /**
   *
   * @param Keys $keys
   * @return self
   */
  public function merge(Keys $keys): self {
    $this->keys = array_merge($this->keys, $keys->toArray());

    return $this;
  }

  /**
   *
   * @return Key[]
   */
  public function jsonSerialize() {
    return $this->keys;
  }
}
