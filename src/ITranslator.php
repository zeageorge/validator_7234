<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

/**
 * ITranslator
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
interface ITranslator {
  /**
   * eg.
   *  echo $obj->translate('Hello {name}', ['{name}' => 'George']); // return 'Hello George'
   *
   * @param string $text The text to translate
   * @param array $placeholders text to replace
   * @return string The translated text
   * @throws TextNotFoundException
   */
  public function translate(string $text, array $placeholders = []): string;
}
