<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use function is_int, ctype_digit;

/**
 * Description of IntVal
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class IntVal extends BaseRule {
  const DEFAULT_ERROR_MESSAGE = 'Not a valid integer';

  /**
   * Constructor
   *
   */
  public function __construct() {
    parent::__construct(['name' => 'integer_value']);

    $this->error->setCode(self::class)->setMessage($this->translator->translate(self::DEFAULT_ERROR_MESSAGE));
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    if (is_int($input)) {
      return true;
    }

    return ctype_digit($input);
  }
}
