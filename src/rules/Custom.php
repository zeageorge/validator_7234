<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use InvalidArgumentException;
use function is_callable;

/**
 * Description of Custom
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Custom extends BaseRule {
  /** @var callable */
  protected $validator = null;

  /**
   * Constructor
   *
   * @param callable $validator
   * @throws InvalidArgumentException
   */
  public function __construct($validator = null) {
    parent::__construct(['name' => 'custom']);

    $this->error->setCode(self::class);

    $this->setValidator($validator ?? function ($input, self $ctx): bool {return false;});
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    return (bool) (($this->validator)($input, $this));
  }

  /**
   *
   * @return callable
   */
  public function getValidator(): callable {
    return $this->validator;
  }

  /**
   *
   *
   * @param callable $validator
   * @throws InvalidArgumentException
   */
  public function setValidator($validator): self {
    if (!is_callable($validator)) {
      throw new InvalidArgumentException('[validator] must be a callable');
    }

    $this->validator = $validator;

    return $this;
  }
}
