<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use InvalidArgumentException;
use zeageorge\errors_7234\Error;
use Countable as CountableInterface;
use function
  count,
  get_object_vars,
  is_array,
  is_int,
  is_object,
  is_string,
  mb_detect_encoding,
  mb_strlen;

/**
 * Description of Length
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Length extends BaseRule {
  const MIN_MAX_ERROR_MESSAGE = 'Value must be between {min_value} and {max_value} {type}';
  const MIN_ERROR_MESSAGE = "Value can't be less than {min_value}";
  const MAX_ERROR_MESSAGE = "Value can't be more than {max_value}";

  /** @var string */
  protected $type = '';

  /** @var Error */
  protected $min_error;

  /** @var Error */
  protected $max_error;

  /** @var Error */
  protected $min_max_error;

  /** @var int|null */
  protected $min_value = null;

  /** @var int|null */
  protected $max_value = null;

  /** @var bool */
  protected $inclusive = true;

  /**
   *
   * @param int|null $min_value
   * @param int|null $max_value
   * @param bool $inclusive
   * @throws InvalidArgumentException
   */
  public function __construct(array $params = []) {
    parent::__construct(['name' => 'length']);

    foreach ($params + [
      'min_value' => null,
      'max_value' => null,
      'inclusive' => true,
    ] as $key => $value) {
      $this->$key = $value;
    }

    if ($this->max_value !== null && $this->min_value > $this->max_value) {
      $error_msg = $this->translator->translate('{max_value} cannot be less than {min_value} for validation', ['{max_value}' => $this->max_value, '{min_value}' => $this->min_value]);
      throw new InvalidArgumentException($error_msg);
    }

    $this->min_error = new Error(['code' => self::class, 'message' => $this->translator->translate(self::MIN_ERROR_MESSAGE)]);
    $this->max_error = new Error(['code' => self::class, 'message' => $this->translator->translate(self::MAX_ERROR_MESSAGE)]);
    $this->min_max_error = new Error(['code' => self::class, 'message' => $this->translator->translate(self::MIN_MAX_ERROR_MESSAGE)]);
  }

  /**
   *
   * @return Error|null
   */
  public function getMinError(): ?Error {
    return $this->min_error;
  }

  /**
   *
   * @param Error $error
   * @return self
   */
  public function setMinError(Error $error): self {
    $this->min_error = $error;

    return $this;
  }

  /**
   *
   * @return Error|null
   */
  public function getMaxError(): ?Error {
    return $this->max_error;
  }

  /**
   *
   * @param Error $error
   * @return self
   */
  public function setMaxError(Error $error): self {
    $this->max_error = $error;

    return $this;
  }

  /**
   *
   * @return Error|null
   */
  public function getMinMaxError(): ?Error {
    return $this->min_max_error;
  }

  /**
   *
   * @param Error $error
   * @return self
   */
  public function setMinMaxError(Error $error): self {
    $this->min_max_error = $error;

    return $this;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    $length = $this->extractLength($input);

    if ($length === null) {
      return false;
    }

    $min_validation = $this->validateMin($length);
    $max_validation = $this->validateMax($length);

    $interpolations = ['{min_value}' => $this->min_value, '{max_value}' => $this->max_value, '{type}' => $this->type];

    !$min_validation && $this->setError($this->min_error->setMessage($this->translator->translate($this->min_error->getMessage(), $interpolations)));
    !$max_validation && $this->setError($this->max_error->setMessage($this->translator->translate($this->max_error->getMessage(), $interpolations)));
    !$min_validation && !$max_validation && $this->setError($this->min_max_error->setMessage($this->translator->translate($this->min_max_error->getMessage(), $interpolations)));

    return $min_validation && $max_validation;
  }

  /**
   *
   * @param mixed $input
   */
  protected function extractLength($input): ?int {
    $this->type = '';

    if (is_string($input)) {
      $this->type = $this->translator->translate('characters');
      return (int) mb_strlen($input, (string) mb_detect_encoding($input));
    }

    if (is_array($input) || $input instanceof CountableInterface) {
      $this->type = $this->translator->translate('items');
      return count($input);
    }

    if (is_object($input)) {
      return $this->extractLength(get_object_vars($input));
    }

    if (is_int($input)) {
      return $this->extractLength((string) $input);
    }

    return null;
  }

  protected function validateMin(int $length): bool {
    if ($this->min_value === null) {
      return true;
    }

    if ($this->inclusive) {
      return $length >= $this->min_value;
    }

    return $length > $this->min_value;
  }

  protected function validateMax(int $length): bool {
    if ($this->max_value === null) {
      return true;
    }

    if ($this->inclusive) {
      return $length <= $this->max_value;
    }

    return $length < $this->max_value;
  }
}
