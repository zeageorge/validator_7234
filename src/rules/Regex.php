<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use function preg_match, is_scalar;

/**
 * Description of Regex
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Regex extends BaseRule {
  /** @var string */
  protected $regex;

  /**
   *
   * @param string $regex
   */
  public function __construct(string $regex) {
    parent::__construct(['name' => 'regex']);

    // $this->error->setCode(str_replace('\\\\', '\\', self::class));
    $this->error->setCode(self::class);
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    if (!is_scalar($input)) {
      return false;
    }

    return preg_match($this->regex, (string) $input) > 0;
  }

  /**
   *
   * @return string
   */
  public function getRegex(): string {
    return $this->regex;
  }

  /**
   *
   * @param string $regex
   * @return self
   */
  public function setRegex(string $regex): self {
    $this->regex = $regex;

    return $this;
  }
}
