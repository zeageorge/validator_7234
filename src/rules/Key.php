<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use InvalidArgumentException;
use zeageorge\errors_7234\{Errors, Error};
use zeageorge\validator_7234\{Validatables, Validatable};

/**
 * Description of Key
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Key extends BaseRule {
  const MISSING_ERROR = 'This field is required';

  /** @var Validatables */
  protected $validatables;

  /** @var Error */
  protected $missing_error;

  /** @var bool */
  protected $mandatory;

  /** @var Errors */
  protected $errors;

  /** @var string */
  protected $key_name;

  /**
   * Constructor
   *
   * @param string $name the key name in the collection
   * @param Validatable ...$validatables
   * @throws InvalidArgumentException
   */
  public function __construct(string $key_name, Validatable ...$validatables) {
    parent::__construct(['name' => 'key']);

    if (empty($key_name)) {
      throw new InvalidArgumentException($this->translator->translate("[key_name] can't be empty"));
    }

    $this->key_name = $key_name;
    
    $this->mandatory = true;

    $this->validatables = new Validatables(...$validatables);

    $this->errors = new Errors();

    $this->missing_error = new Error(['code' => __NAMESPACE__ . '\Required', 'data' => ['field_name' => $key_name], 'message' => $this->translator->translate(self::MISSING_ERROR)]);
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    $this->errors->clear();

    foreach ($this->getRules()->toArray() as $validatable) {
      if (!$validatable->validate($input)) {
        $this->errors->add(($validatable->getError())->setData(['field_name' => $this->key_name]));

        if (!$validatable->getContinue()) {
          break;
        }
      }
    }

    return $this->errors->count() < 1;
  }

  /**
   *
   * @return string
   */
  public function getKeyName(): string {
    return $this->key_name;
  }

  /**
   *
   * @return bool
   */
  public function isMandatory(): bool {
    return $this->mandatory;
  }

  /**
   *
   * @param bool $mandatory
   * @return self
   */
  public function setMandatory(bool $mandatory): self {
    $this->mandatory = $mandatory;

    return $this;
  }

  /**
   *
   * @return Error
   */
  public function getMissingError(): Error {
    return $this->missing_error;
  }

  /**
   *
   * @param Error $error
   * @return self
   */
  public function setMissingError(Error $missing_error): self {
    $this->missing_error = $missing_error;

    return $this;
  }

  /**
   *
   * @return Validatables
   */
  public function getRules(): Validatables {
    return $this->validatables;
  }

  /**
   *
   * @return Errors
   */
  public function getErrors(): Errors {
    return $this->errors;
  }
}
