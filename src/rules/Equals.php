<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

/**
 * Description of Equals
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Equals extends BaseRule {
  const DEFAULT_ERROR_MESSAGE = 'Not equal';

  /** @var mixed */
  protected $compare_to = null;

  /** @var bool */
  protected $compare_identical = false;

  /**
   * Constructor
   *
   * @param mixed $compare_to
   * @param bool $compare_identical
   */
  public function __construct($compare_to, bool $compare_identical = false) {
    parent::__construct(['name' => 'equals']);

    $this->compare_to = $compare_to;

    $this->compare_identical = $compare_identical;

    $this->error->setCode(self::class)->setMessage($this->translator->translate(self::DEFAULT_ERROR_MESSAGE));
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    return $this->compare_identical ? $input === $this->compare_to : $input == $this->compare_to;
  }
}
