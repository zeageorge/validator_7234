<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use function
  in_array,
  is_array,
  mb_detect_encoding,
  mb_stripos,
  mb_strpos;

/**
 * Description of In
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class In extends BaseRule {
  const DEFAULT_ERROR_MESSAGE = 'Not allowed';

  /** @var mixed[]|mixed */
  protected $haystack;

  /** @var bool */
  protected $compareIdentical = false;

  /**
   * Constructor
   *
   * @param mixed[]|mixed $haystack
   * @param bool $compareIdentical
   */
  public function __construct($haystack, bool $compareIdentical = false) {
    parent::__construct(['name' => 'in']);

    $this->haystack = $haystack;

    $this->compareIdentical = $compareIdentical;

    // $this->error->setCode(str_replace('\\\\', '\\', self::class));
    $this->error->setCode(self::class)->setMessage($this->translator->translate(self::DEFAULT_ERROR_MESSAGE));
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    return $this->compareIdentical ? $this->validateIdentical($input) : $this->validateEquals($input);
  }

  /**
   *
   * @param mixed $input
   */
  private function validateEquals($input): bool {
    if (is_array($this->haystack)) {
      return in_array($input, $this->haystack);
    }

    if ($input === null || $input === '') {
      return $input == $this->haystack;
    }

    $inputString = (string) $input;

    return mb_stripos($this->haystack, $inputString, 0, (string) mb_detect_encoding($inputString)) !== false;
  }

  /**
   *
   * @param mixed $input
   */
  private function validateIdentical($input): bool {
    if (is_array($this->haystack)) {
      return in_array($input, $this->haystack, true);
    }

    if ($input === null || $input === '') {
      return $input === $this->haystack;
    }

    $inputString = (string) $input;

    return mb_strpos($this->haystack, $inputString, 0, (string) mb_detect_encoding($inputString)) !== false;
  }
}
