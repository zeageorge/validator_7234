<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use function filter_var, is_string;
use const FILTER_VALIDATE_EMAIL;

/**
 * Description of Email
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Email extends BaseRule {
  const DEFAULT_ERROR_MESSAGE = 'Not a valid email address';

  /**
   * Constructor
   *
   */
  public function __construct() {
    parent::__construct(['name' => 'email']);

    // $this->error->setCode(str_replace('\\\\', '\\', self::class));
    $this->error->setCode(self::class)->setMessage($this->translator->translate(self::DEFAULT_ERROR_MESSAGE));
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    if (!is_string($input)) {
      return false;
    }

    return (bool) filter_var($input, FILTER_VALIDATE_EMAIL);
  }
}
