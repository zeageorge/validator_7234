<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use zeageorge\errors_7234\Error;
use zeageorge\validator_7234\{Validatable, ITranslator, Translator};
use function
  substr,
  strrchr,
  strtolower,
  get_class;

/**
 * Description of BaseRule
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class BaseRule implements Validatable {
  const DEFAULT_ERROR_MESSAGE = 'Validation failed';

  /** @var ITranslator|null */
  protected $translator = null;

  /** @var string|null */
  protected $name = null;

  /** @var Error|null */
  protected $error = null;

  /** @var bool */
  protected $continue = true;

  /**
   * Constructor
   *
   * @param ITranslator $translator default is zeageorge\validator_7234\Translator
   * @param string $name
   * @param Error $error
   * @param bool $continue
   */
  public function __construct(array $params = []) {
    foreach ($params + [
      'translator' => null,
      'name' => null,
      'error' => null,
      'continue' => true,
    ] as $key => $value) {
      $this->$key = $value;
    }

    $this->translator = $this->translator ?? new Translator();

    $this->error = $this->error ?? new Error(['code' => self::class, 'message' => $this->translator->translate(self::DEFAULT_ERROR_MESSAGE)]);

    $this->name = $this->name ?? strtolower(substr(strrchr(get_class($this), '\\'), 1));
  }

  /**
   *
   * {@inheritDoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function setName(string $name): Validatable {
    $this->name = $name;

    return $this;
  }

  /**
   *
   *{@inheritDoc}
   */
  public function getError(): ?Error {
    return $this->error;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function setError(Error $error): Validatable {
    $this->error = $error;

    return $this;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    return false;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function getContinue(): bool {
    return $this->continue;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function setContinue(bool $v): Validatable {
    $this->continue = $v;

    return $this;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function getTranslator(): ITranslator {
    return $this->translator;
  }

  /**
   *
   * {@inheritDoc}
   */
  public function setTranslator(ITranslator $translator): Validatable {
    $this->translator = $translator;

    return $this;
  }
}
