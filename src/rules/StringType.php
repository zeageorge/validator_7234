<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use function is_string;

/**
 * Description of StringType
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class StringType extends BaseRule {
  const DEFAULT_ERROR_MESSAGE = 'Not a valid string';

  /**
   * Constructor
   *
   */
  public function __construct() {
    parent::__construct(['name' => 'string_type']);

    $this->error->setCode(self::class)->setMessage($this->translator->translate(self::DEFAULT_ERROR_MESSAGE));
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    return is_string($input);
  }
}
