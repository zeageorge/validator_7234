<?php

declare(strict_types=1);

namespace zeageorge\validator_7234\rules;

use zeageorge\errors_7234\{Errors, Error};
use zeageorge\validator_7234\Keys;
use function
  count,
  is_array,
  array_reduce,
  array_merge,
  array_key_exists;

/**
 * Description of KeySet
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class KeySet extends BaseRule {
  /** @var Keys */
  protected $keys;

  /** @var Errors[] [string => Errors] */
  protected $errors = [];

  /**
   * Constructor
   *
   * @param Key ...$keys
   */
  public function __construct(Key ...$keys) {
    parent::__construct(['name' => 'keyset']);

    $this->keys = new Keys(...$keys);
  }

  /**
   *
   * {@inheritDoc}
   */
  public function validate($input): bool {
    parent::validate($input);

    $this->errors = [];

    if (!is_array($input)) {
      $this->errors['input'] = new Errors(new Error(['message' => $this->translator->translate(self::DEFAULT_ERROR_MESSAGE)]));
      return false;
    }

    foreach ($this->getKeys()->toArray() as $key) {
      /** @var string */
      $key_name = $key->getKeyName();

      /** @var bool */
      $key_exists = array_key_exists($key_name, $input);

      if (!$key_exists && $key->isMandatory()) {
        if (!array_key_exists($key_name, $this->errors)) {
          $this->errors[$key_name] = new Errors();
        }

        $this->errors[$key_name]->add($key->getMissingError());

        if (!$key->getContinue()) {
          break;
        }
      } elseif ($key_exists && !$key->validate($input[$key_name])) {
        if (!array_key_exists($key_name, $this->errors)) {
          $this->errors[$key_name] = new Errors();
        }

        $this->errors[$key_name]->merge($key->getErrors());

        if (!$key->getContinue()) {
          break;
        }
      }
    }

    return count($this->errors) < 1;
  }

  /**
   *
   * @return Keys
   */
  public function getKeys(): Keys {
    return $this->keys;
  }

  /**
   *
   * @return Error[]
   */
  public function getErrors(): array {
    return array_reduce($this->errors, function (array $carry, Errors $errors) {
      return array_merge($carry, $errors->toArray());
    }, []);
  }
}
