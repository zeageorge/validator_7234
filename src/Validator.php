<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

use zeageorge\errors_7234\{Errors, Error};
use zeageorge\validator_7234\rules\{KeySet, Key};

/**
 * Description of Validator
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Validator {
  /** @var Errors */
  protected $errors;

  /** @var Validatables */
  protected $rules;
  
  /** @var ITranslator */
  protected $translator;

  /**
   *
   * @param Validatable ...$rules
   */
  public function __construct(Validatable ...$rules) {
    $this->errors = new Errors();

    $this->translator = new Translator();

    $this->rules = new Validatables(...$rules);
  }

  /**
   *
   * @param mixed $input
   * @return bool
   */
  public function validate($input): bool {
    $this->errors->clear();

    foreach ($this->getRules()->toArray() as $rule) {
      if (!$rule->validate($input)) {
        if ($rule instanceof KeySet) {
          $this->errors->add(...$rule->getErrors());
        } elseif ($rule instanceof Key) {
          $this->errors->merge($rule->getErrors());
        } else {
          $this->errors->add($rule->getError());
        }

        if (!$rule->getContinue()) {
          break;
        }
      }
    }

    return $this->errors->count() < 1;
  }

  /**
   *
   * @return Validatables
   */
  public function getRules(): Validatables {
    return $this->rules;
  }

  /**
   *
   * @return Errors
   */
  public function getErrors(): Errors {
    return $this->errors;
  }

  /**
   *
   * @return ITranslator
   */
  public function getTranslator(): ITranslator {
    return $this->translator;
  }

  /**
   *
   * @param ITranslator $translator
   * @return self
   */
  public function setTranslator(ITranslator $translator): self {
    $this->translator = $translator;

    return $this;
  }
}
