<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

use zeageorge\errors_7234\Error;
use zeageorge\validator_7234\ITranslator;

/**
 * Description of Validatable
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
interface Validatable {
  /**
   *
   * @return string
   */
  public function getName(): string;

  /**
   *
   * @param string $name
   * @return Validatable
   */
  public function setName(string $name): Validatable;

  /**
   *
   * @return Error|null
   */
  public function getError(): ?Error;

  /**
   *
   * @param Error $error
   * @return Validatable
   */
  public function setError(Error $error): Validatable;

  /**
   * @param mixed $input
   * @return bool
   */
  public function validate($input): bool;

  /**
   *
   * @return bool
   */
  public function getContinue(): bool;

  /**
   *
   * @param bool $v
   * @return Validatable
   */
  public function setContinue(bool $v): Validatable;

  /**
   *
   * @return ITranslator
   */
  public function getTranslator(): ITranslator;

  /**
   *
   * @param ITranslator $translator
   * @return self
   */
  public function setTranslator(ITranslator $translator): Validatable;
}
