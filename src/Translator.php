<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

use function
  array_values,
  array_keys,
  str_replace;

/**
 * The default translator
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Translator implements ITranslator {
  /**
   *
   * {@inheritDoc}
   */
  public function translate(string $text, array $placeholders = []): string {
    return str_replace(array_keys($placeholders), array_values($placeholders), $text);
  }
}
