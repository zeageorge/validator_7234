<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

use JsonSerializable, Countable;
use function count, array_merge;

/**
 * Description of Validatables
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Validatables implements JsonSerializable, Countable {
  /** @var Validatable[] */
  protected $validatables;

  /**
   * Constructor
   *
   * @param Validatable ...$validatables
   */
  public function __construct(Validatable ...$validatables) {
    $this->validatables = $validatables ?? [];
  }

  /**
   *
   * @return Validatable[]
   */
  public function toArray(): array {
    return $this->validatables;
  }

  /**
   *
   * @param Validatable ...$validatables
   * @return self
   */
  public function add(Validatable ...$validatables): self {
    return $this->merge(new Validatables(...$validatables));
  }

  /**
   *
   * @return self
   */
  public function clear(): self {
    $this->validatables = [];

    return $this;
  }

  /**
   *
   * @return int
   */
  public function count(): int {
    return count($this->validatables);
  }

  /**
   *
   * @param Validatables $validatables
   * @return self
   */
  public function merge(Validatables $validatables): self {
    $this->validatables = array_merge($this->validatables, $validatables->toArray());

    return $this;
  }

  /**
   *
   * @return Validatable[]
   */
  public function jsonSerialize() {
    return $this->validatables;
  }
}
