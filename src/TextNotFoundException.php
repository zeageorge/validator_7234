<?php

declare(strict_types=1);

namespace zeageorge\validator_7234;

use Exception;

/**
 * TextNotFoundException
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class TextNotFoundException extends Exception {
}
