# Validator

Simple form validator

Fork for use with php version >=7.2.34

## Install

Via Composer

``` bash
$ composer require zeageorge/validator_7234
```

## Usage

This package requires zeageorge\errors_7234, so you must first install it in order to use the following example.


``` php
<?php

declare(strict_types=1);

require 'vendor/autoload.php';

use zeageorge\errors_7234\Error;
use zeageorge\validator_7234\{Validator, Validatables};
use zeageorge\validator_7234\rules\{Key, KeySet, Length, StringType, Custom, Equals, Email};
use function count, var_dump, array_map, is_string, array_reduce, preg_match;

$request_data = [
  'first_name' => 'George',
  'last_name' => 'Zeakis',
  'email_address' => 'zeageorge@gmail.com',
  'passwords' => '',
  'agree' => 'yes',
];

/** @var Error[] */
$validation_errors = validateRequestData($request_data);

if (count($validation_errors)) {
  var_dump($validation_errors);
}

/**
 *
 * @param array|null $request_data
 * @return Error[]
 */
function validateRequestData(?array $request_data): array {
  /** @var Error[] $errors */
  $errors = [];

  if (!$request_data) {
    $errors[] = new Error(['code' => 'CODE_REQUEST_VALIDATION_FAILED', 'message' => "Request validation failed: request body is not a valid json string"]);
    return $errors;
  }

  $agree = new Key('agree', ...getAgreeRules()->toArray());
  $agree->getMissingError()->setMessage('You must accept our privacy policy and terms of service by click the above checkbox in order to register');

  $validator = new Validator(new KeySet(
    new Key('email_address', ...getEmailRules()->toArray()),
    new Key('password', ...getPasswordRules()->toArray()),
    $agree,
    (new Key('first_name', (new StringType())->setContinue(false), new Length(['min_value' => 0, 'max_value' => 128])))->setMandatory(false),
    (new Key('last_name', (new StringType())->setContinue(false), new Length(['min_value' => 0, 'max_value' => 128])))->setMandatory(false),
  ));

  $validator->validate($request_data);

  return $validator->getErrors()->toArray();
}

/**
 *
 * @return Validatables
 */
function getEmailRules(): Validatables {
  $min_len = 5;
  $max_len = 128;

  $length = (new Length(['min_value' => $min_len, 'max_value' => $max_len]))->setContinue(false);
  $length_error_message = 'The email address must be between {min_len} and {max_len} characters long';
  array_map(function (string $method) use ($length, $length_error_message) {
    return $length->{$method}()->setMessage($length_error_message);
  }, ['getMinError', 'getMaxError', 'getMinMaxError']);

  $email = new Email();
  $email->getError()->setMessage("A valid email address is required");

  return new Validatables((new StringType())->setContinue(false), $length, $email);
}

/**
 *
 * @return Validatables
 */
function getPasswordRules(): Validatables {
  $min_len = 5;
  $max_len = 128;
  $regex_groups = ['/[A-Z]/', '/[a-z]/', '/[0-9]/', '/[\(\)\[\{\*\+\.\$\^\\\|\?~!@#%&_}:"<>`\-\]=;\',]/'];
  $min_required_groups = 3;

  $length = (new Length(['min_value' => $min_len, 'max_value' => $max_len]))->setContinue(false);
  $length_error_message = 'The password must be between {min_len} and {max_len} characters long';
  array_map(function (string $method) use ($length, $length_error_message) {
    return $length->{$method}()->setMessage($length_error_message);
  }, ['getMinError', 'getMaxError', 'getMinMaxError']);

  $cb = function ($input, Custom $ctx) use ($regex_groups, $min_required_groups): bool {
    return is_string($input) && array_reduce($regex_groups, function (int $count, string $regex) use ($input): int {
      if (preg_match($regex, $input) === 1) {
        $count++;
      }
      return $count;
    }, 0) > $min_required_groups - 1;
  };

  $custom = new Custom($cb);

  $custom->getError()->setMessage('The new password must be between {length_min} and {length_max} characters long and it must contain at least three of the four available character types: lowercase letters {low}, uppercase letters {upp}, numbers {num}, and symbols {symbols}');

  return new Validatables((new StringType())->setContinue(false), $length, $custom);
}

/**
 *
 * @return Validatables
 */
function getAgreeRules(): Validatables {
  $equals = new Equals(['compare_to' => 'yes']);

  $equals->getError()->setMessage('You must accept our privacy policy and terms of service by click the above checkbox in order to register');

  return new Validatables((new StringType())->setContinue(false), $equals);
}


```

